# alanGrant
[![pipeline status](https://gitlab.com/filipefreire/alanGrant/badges/master/pipeline.svg)](https://gitlab.com/filipefreire/alanGrant/commits/master)
[![coverage report](https://gitlab.com/filipefreire/alanGrant/badges/master/coverage.svg)](https://gitlab.com/filipefreire/alanGrant/commits/master)

## Simple memory refresher for Ruby

- [x] split a string + unit tests
- [ ] bundle it up
- [x] fizz buzz + unit tests
- [x] fibonacci + unit tests
- [ ] coverage
- [x] Gitlab CI - build and test
- [x] badges on README

## SplitString & FizzBuz & Fibonacci

(Dependencies: have Ruby installed, and also rubocop)

To run tests:

```
ruby split_string_spec.rb
```

To run static analysis:

```
rubocop .
```
