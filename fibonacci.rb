# File: fibonacci.rb

# Fibonacci of a number (max. 30 - safe integer limit)
class Fibonacci
  def initialize(number)
    raise unless number.is_a?(Integer)
    raise unless number >= 0 && number <= 30
    @number = number
  end

  def as_value
    if @number.zero?
      0
    elsif @number == 1
      1
    elsif @number == 2
      1
    else
      Fibonacci.new(@number - 1).as_value + Fibonacci.new(@number - 2).as_value
    end
  end
end
