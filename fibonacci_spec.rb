# File: fibonacci_spec.rb

require_relative 'fibonacci'
require 'test/unit'

# Test Fibonacci class
class TestFibonacci < Test::Unit::TestCase
  def test_no_argument
    assert_raise ArgumentError do
      Fibonacci.new
    end
  end

  def test_not_integer
    assert_raise do
      Fibonacci.new('a')
    end
  end

  def test_less_than_zero
    assert_raise do
      Fibonacci.new(-1)
    end
  end

  def test_more_than_max_limit
    assert_raise do
      Fibonacci.new(31)
    end
  end

  def test_zero
    assert_equal(0, Fibonacci.new(0).as_value)
  end

  def test_one
    assert_equal(1, Fibonacci.new(1).as_value)
  end

  def test_two
    assert_equal(1, Fibonacci.new(2).as_value)
  end

  def test_ten
    assert_equal(55, Fibonacci.new(10).as_value)
  end

  def test_eighty_three
    assert_equal(832_040, Fibonacci.new(30).as_value)
  end
end
