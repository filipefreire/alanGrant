# File:  fizz_buzz.rb

# FizzBuzzOf class, returns fizzbuzz array given number, or prints it
class FizzBuzzOf
  def initialize(number)
    raise unless number.is_a?(Integer)
    raise unless number >= 0
    @number = number
  end

  def as_array
    [*0..@number].map do |n|
      str = ''
      (n % 3).zero? ? str += 'Fizz' : ''
      (n % 5).zero? ? str += 'Buzz' : ''
      str
    end
  end

  def print
    as_array.each_with_index do |n, index|
      puts index.to_s + ':' + n
    end
  end
end
