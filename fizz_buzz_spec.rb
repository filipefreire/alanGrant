# File:  fizz_buzz_spec.rb

require_relative 'fizz_buzz'
require 'test/unit'

# Tests for FizzBuzzOf class
class TestFizzBuzzOf < Test::Unit::TestCase
  def test_no_arguments
    assert_raise ArgumentError do
      FizzBuzzOf.new
    end
  end

  def test_invalid_arguments
    assert_raise do
      FizzBuzzOf.new('2')
    end
    assert_raise do
      FizzBuzzOf.new(-1)
    end
  end

  def test_valid_arguments
    assert_equal(
      ['FizzBuzz', '', '', 'Fizz', '', 'Buzz'],
      FizzBuzzOf.new(5).as_array
    )
  end

  def test_valid_arguments_zero
    assert_equal(
      ['FizzBuzz'],
      FizzBuzzOf.new(0).as_array
    )
  end
end
