# File:  string_split.rb

# SplitString class - for spliting a string given a delimiter
class SplitString
  def initialize(str, delimiter)
    raise unless str.is_a?(String)
    raise unless delimiter.is_a?(String)
    @str = str
    @delimiter = delimiter
  end

  def as_array
    @str.split(@delimiter)
  end

  def print
    asArray.each { |str| puts '´' + str + '´' }
  end
end
