# File:  string_split_spec.rb

require_relative 'string_split'
require 'test/unit'

# Tests for SplitString class
class TestSplitString < Test::Unit::TestCase
  def test_no_arguments
    assert_raise ArgumentError do
      SplitString.new
    end
  end

  def test_wrong_number_of_arguments
    assert_raise ArgumentError do
      SplitString.new(2)
    end
  end

  def test_invalid_arguments
    assert_raise do
      SplitString.new('2', 2)
    end
    assert_raise do
      SplitString.new(2, '2')
    end
  end

  def test_valid_arguments
    assert_equal(%w[a b c], SplitString.new('a,b,c', ',').as_array)
  end

  def test_valid_arguments_diferent_delimiter
    assert_equal(['a,b,c'], SplitString.new('a,b,c', '/').as_array)
  end

  def test_empty_string
    assert_equal([], SplitString.new('', '/').as_array)
  end

  def test_empty_delimiter
    assert_equal(
      ['a', ',', 'b', ',', 'c'],
      SplitString.new('a,b,c', '').as_array
    )
  end
end
